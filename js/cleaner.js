$(document).ready(function() {
    var title = undefined;
    var content = undefined; 
    var author_txt = undefined;
    var prevPage = undefined;
    var nextPage = undefined;
    var bookId = undefined;
    var chapterId = undefined;
    var bookMarkUrl = undefined
    var isVip = false;
        
    var parseParam = function() {
        var pathname = location.pathname;
        var filename = pathname.substr(pathname.lastIndexOf('/'));
        var id_arr = filename.split(/[\/\,\].]+/);
        isVip = id_arr[1] === 'vip' ? true : false;
        id_arr = id_arr.filter(function(item) {
            return /[\d]+/.test(item);
        });
        bookId = id_arr[0];
        chapterId = id_arr[1];
    };

    var getBook = function() {
        if (isVip) {
            bookMarkUrl = "/Ajax.aspx";
            title = $("#lbChapterName").clone().html();
            content = $("#wdvscontent").clone().html();
            // the latest chapter will show image
            if (content === undefined) {
                content = $("img");
                content.attr("src", $("#Image1").attr("src"));
            } 
            author_txt = $(".author_txt").clone().html();
            prevPage = $("#PrevPage").clone().attr("href");
            nextPage = $("#NextPage").clone().attr("href");
        } else {
            bookMarkUrl = "/Ajax/AjaxCom.php";
            title = $("#lbChapterName").clone().html();
            // should script, because it will make our script fail
            $("script").remove();
            // clean ads in content
            $("#content > span").remove();
            $("#content > div[id^=div]").remove();
            content = $("#content").clone().html();
            prevPage = $("#BottomPrevLink").clone().attr("href");
            nextPage = $("#BottomNextLink").clone().attr("href");
        }
    };

    var addBookMark = function() {
        $.get(bookMarkUrl, {
            "opName": "addBookMark", 
            "bookId": bookId, 
            "chapterId": chapterId
        }).done(function(data) {
                $("#bookmark").removeClass("red");
                $("#bookmark").addClass("green");
        }).fail(function(data) {
                $("#bookmark").removeClass("green");
                $("#bookmark").addClass("red");
        });
    };


    var cleanElement = function() {
        $("html").empty();
    };

    var render = function() {
        // it should be sync so that make html content be global
        $.ajax({
            url: chrome.extension.getURL("/html/template.html"),
            async: false,
            success: function(template) {
                $("html").html(template);
                $("#bookmark").click(function() {
                    addBookMark();
                });
            }
        });
    };

    var injectbook = function() {
        $("#title").html(title);
        $("#content").html(content);
        if (author_txt) 
            $("#author_txt").html(author_txt);
        else
            $("#author_txt").remove();
       
        $("#prev").attr("href", prevPage);
        $("#next").attr("href", nextPage);
    };

    // main
    chrome.storage.sync.get("enable", function(e) {
        var isEnable = e["enable"];
        if (e["enable"] === undefined) {
            isEnable = true;
            chrome.storage.sync.set({"enable": true});
        }

        if (isEnable) {
            parseParam();
            getBook();
            cleanElement();
            render();
            injectbook();
        }
    });
});
