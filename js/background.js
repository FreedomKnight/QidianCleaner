chrome.browserAction.onClicked.addListener(function() {
    chrome.storage.sync.get("enable", function(e) {
        var isEnable = e["enable"];
        if (isEnable === undefined) {
            chrome.storage.sync.set({"enable": true});
            isEnable = true;
        } else if (isEnable) {
            chrome.storage.sync.set({"enable": false});
            isEnable = false;
        } else {
            chrome.storage.sync.set({"enable": true});
            isEnable = true;
        }

        if (isEnable) {
            chrome.browserAction.setBadgeBackgroundColor({color: [0, 255, 0, 255]});
            chrome.browserAction.setBadgeText({text: "on"});
        } else {
            chrome.browserAction.setBadgeBackgroundColor({color: [255, 0, 0, 255]});
            chrome.browserAction.setBadgeText({text: "off"});
        }

        chrome.tabs.reload();
    });
});
